
//  Created by Nikola Prodanovic RT-18/15

#include<stdio.h>
#include<stdlib.h>
#include<string.h>



#define IME_MOB 20


//  kreiranje niza struktura nizMobilni u koju se
//  unose podaci iz vec kreirane datoteke "mobilni.txt"

struct nizMobilni{
    char nizModel[IME_MOB + 1];
    int nizKolicina;
    float nizCena;
};


//  kreiranje strukture unosMobilni u koju se onose podaci
//  za kupovinu i azuriranje zeljenog modela

struct unosMobilni{
    char unosModel[IME_MOB + 1];
    int unosKolicina;
    float unosCena;

};


//  pomocna struktura

struct pomocnaMobilni {
    char pomocnaModel[20];
    int pomocnaKolicina;
    float pomocnaCena;
};


//  funkcije

void prikaz();              //  pocetni prikaz ekrana
int menu();                 //  biranje opcije
void pocetnaPonuda();       //  pocetna ponuda bez izmena (pocetno stanje datoteke)
int kupovina();             //  kupovina telefona i zuriranje kolicine telefona
void azuriranjeCene();       //  azururanje cene postojecih modela telefona
void unosNovogModela();     //  unos novog modela telefona u datoteku
int kolicinaTelefona();     //  broji kolicinu modela u fajlu



main() {

    int opcija;

    while (1) {


        prikaz();
        opcija = menu();


        switch (opcija)
        {
            case 1:
                pocetnaPonuda();
                break;

            case 2:
                kupovina();
                break;

            case 3:
                azuriranjeCene();
                break;

            case 4:
                unosNovogModela();
                break;

            case 5:
                exit(1);

            default:
                printf("\nIZABRANA NEPOSTOJECA OPCIJA! BIRAJTE PONOVO: \n");
                break;
        }

    }


}

void prikaz() {

    printf("\n");
    printf("  +-----------------------------+\n");
    printf("  |  PRODAJA MOBILNIH TELEFONA  |\n");
    printf("  +-----------------------------+\n");
}

int menu() {

    int opcija;

    printf("  +----------- MENU ------------+\n");
    printf("  |                             |\n");
    printf("  | (1) POCETNA PONUDA          |\n");
    printf("  | (2) KUPOVINA TELEFONA       |\n");
    printf("  | (3) AZURIRANJE CENE         |\n");
    printf("  | (4) UNOS NOVOG MODELA       |\n");
    printf("  | (5) IZLAZ IZ PROGRAMA       |\n");
    printf("  |                             |\n");
    printf("  +-----------------------------+\n");

    printf("\nUnesite broj opcije koju zelite da izaberete: ");
    scanf("%d", &opcija);

    return opcija;
}

void pocetnaPonuda() {


    int i, brojModela = kolicinaTelefona();
    FILE *fptr;
    struct nizMobilni *unosIzFajla;

    //  kreiranje niza struktura za unos podataka iz fajla u niz struktura

    if ((unosIzFajla = malloc(brojModela * sizeof(struct nizMobilni))) == NULL)
    {
        printf("\nGreska pri dodeli memorije!\n");
        exit(1);
    }

    if ((fptr = fopen("mobilni.txt", "r")) == NULL)
    {
        printf("Greska pri otvaranju datoteke!\n");
        exit(1);
    }

    printf("\n");

    printf("  MODEL | KOLICINA | CENA\n\n");

    for (i = 0; i < brojModela; i++)
    {
        fscanf(fptr, "%s %d %f\n", &unosIzFajla[i].nizModel, &unosIzFajla[i].nizKolicina, &unosIzFajla[i].nizCena);
        printf("  %s | %d | %.2f\n", unosIzFajla[i].nizModel, unosIzFajla[i].nizKolicina, unosIzFajla[i].nizCena);

    }

    printf("\n\n");

    fclose(fptr);         //    zatvaranje datoteke
    free(unosIzFajla);    //    oslobadjanje memorije

}

int kupovina() {

    struct unosMobilni unosMobilni;
    struct nizMobilni *nizMobilni;
    struct pomocnaMobilni pomocnaMobilni;
    FILE *fptr;
    int i, postoji, index, brojModela = kolicinaTelefona();

    if ((nizMobilni = malloc(brojModela * sizeof(struct nizMobilni))) == NULL)
    {
        printf("\nGreska pri dodeli memorije!\n");
        exit(1);
    }

    if ((fptr = fopen("mobilni.txt", "r")) == NULL)
    {
        printf("Greska pri otvaranju datoteke!\n");
        exit(1);
    }
    printf("\n");
    printf(" +------------------------+\n");
    printf(" | PONUDA MODELA TELEFONA |\n");
    printf(" +------------------------+\n");
    printf("  MODEL | KOLICINA | CENA\n\n");

    for (i = 0; i < brojModela; i++)
    {
        fscanf(fptr, "%s %d %f\n", &nizMobilni[i].nizModel, &nizMobilni[i].nizKolicina, &nizMobilni[i].nizCena);
        printf("  %s | %d | %.2f\n", nizMobilni[i].nizModel, nizMobilni[i].nizKolicina, nizMobilni[i].nizCena);
    }

    fclose(fptr);

    //unos modela koji korisnik zeli da kupi

    printf("\nUnesite model telefona (UNOS MODELA MALIM SLOVIMA): \n");
    scanf("%s", &unosMobilni.unosModel);

    //poredjenje unesenog modela sa modelom u nizu struktura

    for (i = 0; i < brojModela; i++) {

        if (strcmp(unosMobilni.unosModel, nizMobilni[i].nizModel) == 0) {

            postoji = 1;
            strcpy(pomocnaMobilni.pomocnaModel, nizMobilni[i].nizModel);
            pomocnaMobilni.pomocnaKolicina = nizMobilni[i].nizKolicina;
            index = i;

        }

    }

    //  biranje kolicine modela za kupovinu i azuriranje broja komada u datoteci

    if (postoji == 1) {
        printf("\nKolicina |%s| modela koju mozete kupiti: |%d|", pomocnaMobilni.pomocnaModel, pomocnaMobilni.pomocnaKolicina);

        printf("\nKolicina |%s| modela koju zelite da kupite:  ", pomocnaMobilni.pomocnaModel);
        scanf("%d", &unosMobilni.unosKolicina);

        if(unosMobilni.unosKolicina<=pomocnaMobilni.pomocnaKolicina){

            nizMobilni[index].nizKolicina -= unosMobilni.unosKolicina;

            printf("\nUspesna izvrsena kupovina telefona!\n");
            printf("Preostalo |%s| modela:  |%d| ", pomocnaMobilni.pomocnaModel, nizMobilni[index].nizKolicina);
            printf("\n\n");

        }else {

            printf("Nedovoljna kolicina telefona na stanju!");
        }

    }

    //unos azuriranih podataka u datoteku (azurirana kolicina modela)


    fptr = fopen("mobilni.txt", "w");

    if(fptr == NULL) {

        printf("Greska pri otvaranju datoteke!\n");
        exit(1);
    }

    for (i = 0; i < brojModela; i++) {

        fprintf(fptr, "%s %d %.2f\n", nizMobilni[i].nizModel, nizMobilni[i].nizKolicina, nizMobilni[i].nizCena);
    }

    fclose(fptr);  //zatvaranje datoteke
    free(nizMobilni); //oslobadjanje memorije

    return 0;

}

void azuriranjeCene(){

    struct unosMobilni unosMobilni;
    struct nizMobilni *nizMobilni;
    FILE *fptr;
    int i, brojModela = kolicinaTelefona();

    if ((nizMobilni = malloc(brojModela * sizeof(struct nizMobilni))) == NULL)
    {
        printf("\nGreska pri dodeli memorije!\n");
        exit(1);
    }

    if ((fptr = fopen("mobilni.txt", "r")) == NULL)
    {
        printf("Greska pri otvaranju datoteke!\n");
        exit(1);
    }

    //  prikazivanje ponude telefona

    printf("\n  MODEL | KOMADA | CENA \n");
    for (i = 0; i < brojModela; i++)
    {
        fscanf(fptr, "%s %d %f", &nizMobilni[i].nizModel, &nizMobilni[i].nizKolicina, &nizMobilni[i].nizCena);
        printf("  %s | %d | %.2f\n", nizMobilni[i].nizModel, nizMobilni[i].nizKolicina, nizMobilni[i].nizCena);
    }
    printf("\n");

    fclose(fptr);

    //  unos modela kome se azurira cena
    //  ponovno vracanje ukoliko naziv modela nije unet pravilno

    do{
        printf("\nUnesite model telefona ciju cenu zelite da azurirate\n");
        printf("(UNOS MODELA MALIM SLOVIMA): \n");
        scanf("%s", &unosMobilni.unosModel);

        for (i = 0; i < brojModela; i++)
            if (strcmp(unosMobilni.unosModel, nizMobilni[i].nizModel) == 0)
            {
                printf("\nUnesite novu cenu telefona: ");
                scanf("%f", &unosMobilni.unosCena);

                if(unosMobilni.unosCena>=0){

                    nizMobilni[i].nizCena = unosMobilni.unosCena;

                    printf("\nUspesno izvrseno azuriranje cene!\n");
                    break;
                }else{
                    printf("\nUneta cena je manja od nule!");
                }
            }

    } while (!(strcmp(unosMobilni.unosModel, nizMobilni[i].nizModel) == 0));

    //  upis azuriranih podataka u datoteku

    if ((fptr = fopen("mobilni.txt", "w")) == NULL)
    {
        printf("Greska pri otvaranju datoteke!\n");
        exit(1);
    }

    for (i = 0; i < brojModela; i++)
    {
        fprintf(fptr, "%s %d %.2f\n", nizMobilni[i].nizModel, nizMobilni[i].nizKolicina, nizMobilni[i].nizCena);
    }



    fclose(fptr);           //  zatvaranje datoteke
    free(nizMobilni);       //  oslobadjanje memorije

}

void unosNovogModela() {

    struct unosMobilni unosMobilni;
    FILE *fptr;

    //otvaranje datoteke i unos novog modela u datoteku

    if ((fptr = fopen("mobilni.txt", "a")) == NULL)
    {
        printf("\nGreska pri otvaranju datoteke!\n");
        exit(1);
    }

    printf("\nUnesite model, kolicinu i cenu: \n");
    scanf("%s%d%f", &unosMobilni.unosModel, &unosMobilni.unosKolicina, &unosMobilni.unosCena);

    fprintf(fptr, "%s %d %.0f\n", unosMobilni.unosModel, unosMobilni.unosKolicina, unosMobilni.unosCena);
    printf("\nUnos novog modela uspesno izvrsen!\n");

    fclose(fptr); //zatvaranje datoteke

}

int kolicinaTelefona(){

    FILE *fptr;
    int brojMobilnih=0;

    if ((fptr = fopen("mobilni.txt", "r")) == NULL)
    {
        printf("Greska pri otvaranju datoteke!\n");
        exit(1);
    }

    int pomocnaKolicina;

    do
    {
        pomocnaKolicina = fgetc(fptr);
        if(pomocnaKolicina == '\n')
            brojMobilnih++;
    } while (pomocnaKolicina != EOF);



//    if(pomocnaKolicina != '\n' && brojMobilnih != 0)
//        brojMobilnih++;

    fclose(fptr);
    return brojMobilnih;
}
